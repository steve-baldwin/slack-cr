require "./dotenv"
Dotenv.load display: false

require "slack"
require "logger"

Slack.config do |config|
  config.bot_name = "mybot"
  config.logger = Logger.new(STDOUT).tap do |log|
    log.level = Logger::WARN
    log.formatter = Logger::Formatter.new do |severity, datetime, _, message, io|
      io.printf("%s|%s|%s", severity.to_s[0], datetime.to_s("%H:%M:%S.%6N"), message)
    end
  end
  config.dont_know = "I don't know what to do with that?"
end

Slack.command "help" do
  <<-END
  I understand:
    *help* - This help text
    *say "something"* - Repeat what you say
    *say hi to {user}* - Say hi to the nominated user
    *say "something" to {user}* - Say something to that user

  To get help about a command use `{command} help`.
  END
end

Slack.command "say help" do |ctx, vars|
  <<-END
  Type `say something` and I'll say `something`, or try `say "this long string"` and I'll say that!
  END
end

Slack.command "say \"{something}\"" do |ctx, vars|
  "#{vars["something"].upcase}!"
end

Slack.variable "user", /\<@.+\>/ do |result|
  result
end

Slack.command "say hi to {user}" do |ctx, vars|
  "Hello #{vars["user"]} from <@#{ctx["user_id"]}>!"
end

Slack.command "say \"{something}\" to {user}" do |ctx, vars|
  "#{vars["something"]} #{vars["user"]} from <@#{ctx["user_id"]}>!"
end
