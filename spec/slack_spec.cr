require "spec"
require "./spec_helper"
require "../src/slack"

module Slack
  Spec.before_each do
    Slack.reset
  end

  describe Slack do
    it "works for simple contants" do
      Slack.command "one" do |context|
        "bad1"
      end

      Slack.command "two one" do |context|
        "bad2"
      end

      Slack.command "one two" do |context|
        "good"
      end

      Slack.command "one two two" do |context|
        "bad3"
      end

      Slack.execute(create_args("  one  two ")).should eq "good"
    end

    it "works for single parameter" do
      Slack.variable "test", /\<@.+\>/ do |result|
        result
      end

      Slack.command "give {test}" do |context|
        "good"
      end

      Slack.command "give test" do |context|
        "bad"
      end

      Slack.execute(create_args("give <@2323>")).should eq "good"
    end

    it "works for single parameter with outer constants" do
      Slack.variable "user", /\<@.+\>/ do |result|
        result
      end

      Slack.command "give to {user} because" do |context, vars|
        "#{vars["user"]} text"
      end

      Slack.execute(create_args("give to <@2323> because")).should eq "<@2323> text"
    end

    it "works for multiple parameters" do
      Slack.variable "user", /\<@.+\>/ do |result|
        result
      end

      Slack.variable "award_type", /(delivery|teammate)/ do |result|
        result
      end

      Slack.command "give {user} a {award_type} award" do |context, vars|
        "You gave #{vars["user"]} an award for #{vars["award_type"]}"
      end

      Slack.execute(create_args("give <@2323> a teammate award")).should eq "You gave <@2323> an award for teammate"
    end

    it "works for multiple overlapping commands" do
      Slack.variable "user", /\<@.+\>/ do |result|
        result
      end

      Slack.command "give to {user}" do |context|
        "Test text for first"
      end

      Slack.command "give {user}" do |context|
        "Test text for second"
      end

      Slack.execute(create_args("give <@2323>")).should eq "Test text for second"
    end

    it "works for single and double quoted strings" do
      Slack.command "this \"{string}\" is good" do |context, vars|
        if vars["string"] == "long string"
          next "yep"
        end
        "nup"
      end

      Slack.execute(create_args("this \"long string\" is good")).should eq "yep"
      Slack.execute(create_args("this 'long string' is good")).should eq "yep"
    end

    it "works for smart single and double quoted strings" do
      Slack.command "this \"{string}\" is good" do |context, vars|
        if vars["string"] == "long string"
          next "yep"
        end
        "nup"
      end

      Slack.execute(create_args("this “long string” is good")).should eq "yep"
      Slack.execute(create_args("this ‘long string’ is good")).should eq "yep"
    end

    it "works for this combination of command" do
      Slack.command " award  {user} for {type} because \"{reason}\"" do |context, vars|
        "yep"
      end

      Slack.execute(create_args("award <@WBXGV3XUY|stbaldwin1> for teamwork because “something great”")).should eq "yep"
    end

    it "works for these inputs" do
      Slack.command "award {user} for {type} because \"{reason}\"" do |context, vars|
        "yep"
      end

      inputs = [
        "award <@WBXGV3XUY|stbaldwin1> for teamwork because “something great”",
        " award <@WBXGV3XUY|stbaldwin1> for teamwork because “something great”",
        "award <@WBXGV3XUY|stbaldwin1> for teamwork because “something great” ",
        " award <@WBXGV3XUY|stbaldwin1> for teamwork because “something great” ",
        "award  <@WBXGV3XUY|stbaldwin1>  for  teamwork  because   “something great”",
      ]

      inputs.each do |input|
        Slack.execute(create_args(input)).should eq "yep"
      end
    end

    it "fails when parameter not provided" do
      Slack.variable "user", /\<@.+\>/ do |result|
        result
      end

      Slack.command "give {user}" do |context|
        "Test text"
      end

      Slack.execute(create_args("give")).should eq "Beats me?"
    end

    it "selects the right matching command" do
      Slack.command "one" do |context|
        "first"
      end

      Slack.command "one two three" do |context|
        "second"
      end

      Slack.command "one two" do |context|
        "third"
      end

      Slack.execute(create_args("one two")).should eq "third"
      Slack.execute(create_args("one")).should eq "first"
      Slack.execute(create_args("one two three")).should eq "second"
    end

    it "ignores unimportant stuff" do
      Slack.command "one" do |context|
        "success"
      end

      Slack.execute(create_args("two one two")).should eq "success"
    end
  end
end
