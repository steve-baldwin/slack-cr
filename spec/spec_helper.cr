require "spec"

def create_args(str : String) : Hash(String, String)
  {
    "token"        => "qzYFLvMKpcDtN1pgbhESuRFE",
    "team_id"      => "T0XDMU1TN",
    "team_domain"  => "karagianis",
    "channel_id"   => "C0XDQQPRC",
    "channel_name" => "general",
    "user_id"      => "U0XDW384Q",
    "user_name"    => "steff",
    "command"      => "/kdz",
    "text"         => "#{str}",
    "response_url" => "https://hooks.slack.com/commands/T0XDMU1TN/446465784960/TggSSQNMoCB68FxLbTNMXhCq",
    "trigger_id"   => "448252851655.31463953940.8001cafe48e0111b15ee0efaab4cb2e9",
  }
end
