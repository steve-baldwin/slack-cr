include ./examples/simple/.env
export $(shell sed 's/=.*//' .env)

all: test

build:
	cd examples/simple; time -p crystal build --debug --error-trace --time ./src/simple.cr -o ./bin/simple

run:
	cd examples/simple; time -p crystal ./src/simple.cr

test:
	time -p crystal spec

