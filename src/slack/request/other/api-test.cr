require "../request"

module Slack::Request
  class APITestParams < Params
    property foo : String = "doofus"
    property error : String?
  end

  class APITest < Request
    def initialize
      super("api.test", APITestParams.new)
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
