require "../request"
require "../../response/chat/post-message"

module Slack::Request
  class ChatPostMessageParam < ParamsMessage
    property icon_emoji : String?
    property icon_url : String?
    property mrkdwn = true
    property reply_broadcast = false
    property thread_ts : String?
    property unfurl_links = true
    property unfurl_media = true
    property username : String?
  end

  class ChatPostMessage < Request
    def initialize(params : ChatPostMessageParam)
      super("chat.postMessage", params)
    end

    def initialize(channel : String, text : String? = nil, attachments : Array(Attachment)? = nil)
      initialize(ChatPostMessageParam.new(channel, text, attachments))
    end

    def get_response_class
      Slack::Response::ChatPostMessage
    end
  end
end
