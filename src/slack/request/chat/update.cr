require "../request"
require "../../response/chat/update"

module Slack::Request
  class ChatUpdateParam < ParamsMessage
    property ts : String

    def initialize(@channel, @ts, @text, @attachments)
      super(@channel, @text, @attachments)
    end
  end

  class ChatUpdate < Request
    def initialize(params : ChatUpdateParam)
      super("chat.update", params)
    end

    def initialize(channel : String, ts : String, text : String, attachments : Array(Attachment)? = nil)
      initialize(ChatUpdateParam.new(channel, ts, text, attachments))
    end

    def get_response_class
      Slack::Response::ChatUpdate
    end
  end
end
