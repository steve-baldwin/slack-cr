require "../request"
require "../../response/users/profile-get"

module Slack::Request
  class UsersProfileGetParams < ParamsJSON
    property user : String

    def initialize(@user)
    end
  end

  class UsersProfileGet < Request
    def initialize(user : String)
      super("users.profile.get", UsersProfileGetParams.new(user))
    end

    def get_response_class
      Slack::Response::UsersProfileGet
    end
  end
end
