require "../request"
require "../../response/users/info"

module Slack::Request
  class UsersInfoParams < Params
    property user : String

    def initialize(@user)
    end
  end

  class UsersInfo < Request
    def initialize(user : String)
      super("users.info", UsersInfoParams.new(user))
    end

    def get_response_class
      Slack::Response::UsersInfo
    end
  end
end
