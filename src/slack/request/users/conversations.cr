require "../request"
require "../../response/conversations/list"

module Slack::Request
  class UsersConversationsParams < ParamsJSON
    property cursor : String?
    property exclude_archived = true
    property limit = 20
    property types : String?
    property user : String?

    def initialize(@cursor, @exclude_archived, @limit, @types, @user)
    end
  end

  class UsersConversations < Request
    def initialize(cursor : String? = nil, exclude_archived = true, limit = 20, types : String? = nil, user : String? = nil)
      super("users.conversations", UsersConversationsParams.new(cursor, exclude_archived, limit, types, user))
    end

    def get_response_class
      Slack::Response::ConversationsList
    end
  end
end
