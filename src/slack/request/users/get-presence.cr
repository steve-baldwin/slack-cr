require "../request"
require "../../response/users/get-presence"

module Slack::Request
  class UsersGetPresenceParams < ParamsJSON
    property user : String

    def initialize(@user)
    end
  end

  class UsersGetPresence < Request
    def initialize(user : String)
      super("users.getPresence", UsersGetPresenceParams.new(user))
    end

    def get_response_class
      Slack::Response::UsersGetPresence
    end
  end
end
