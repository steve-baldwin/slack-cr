require "../request"
require "../../response/users/info"

module Slack::Request
  class UsersLookupByEmailParams < Params
    property email : String

    def initialize(@email)
    end
  end

  class UsersLookupByEmail < Request
    def initialize(email : String)
      super("users.lookupByEmail", UsersLookupByEmailParams.new(email))
    end

    def get_response_class
      Slack::Response::UsersInfo
    end
  end
end
