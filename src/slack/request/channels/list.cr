require "../request"

module Slack::Request
  class ChannelsListParams < ParamsJSON
    property cursor : String?
    property exclude_archived = true
    property exclude_members = true
    property limit = 20

    def initialize(@cursor, @exclude_archived, @exclude_members, @limit)
    end
  end

  class ChannelsList < Request
    def initialize(cursor : String? = nil, exclude_archived = true, exclude_members = true, limit = 20)
      @deprecated = true
      raise "This method has been deprecated. Use ConversationsList instead"
      super("channels.list", ChannelsListParams.new(cursor, exclude_archived, exclude_members, limit))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
