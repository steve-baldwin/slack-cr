require "../request"

module Slack::Request
  class ChannelsLeaveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ChannelsLeave < Request
    def initialize(channel : String)
      super("channels.leave", ChannelsLeaveParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
