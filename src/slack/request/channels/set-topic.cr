require "../request"
require "../../response/channels/set-topic"

module Slack::Request
  class ChannelsSetTopicParams < ParamsJSON
    property channel : String
    property topic : String

    def initialize(@channel, @topic)
    end
  end

  class ChannelsSetTopic < Request
    def initialize(channel : String, topic : String)
      super("channels.setTopic", ChannelsSetTopicParams.new(channel, topic))
    end

    def get_response_class
      Slack::Response::ChannelsSetTopic
    end
  end
end
