require "../request"
require "../../response/channels/info"

module Slack::Request
  class ChannelsCreateParams < ParamsJSON
    property name : String
    property validate = true

    def initialize(@name)
    end
  end

  class ChannelsCreate < Request
    def initialize(name : String)
      super("channels.create", ChannelsCreateParams.new(name))
    end

    def get_response_class
      Slack::Response::ChannelsInfo
    end
  end
end
