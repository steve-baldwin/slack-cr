require "../request"

module Slack::Request
  class ChannelsKickParams < ParamsJSON
    property channel : String
    property user : String

    def initialize(@channel, @user)
    end
  end

  class ChannelsKick < Request
    def initialize(channel : String, user : String)
      super("channels.kick", ChannelsKickParams.new(channel, user))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
