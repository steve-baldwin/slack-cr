require "../request"

module Slack::Request
  class ChannelsMarkParams < ParamsJSON
    property channel : String
    property ts : String

    def initialize(@channel, @ts)
    end
  end

  class ChannelsMark < Request
    def initialize(channel : String, ts : String)
      super("channels.mark", ChannelsMarkParams.new(channel, ts))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
