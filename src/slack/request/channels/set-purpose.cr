require "../request"
require "../../response/channels/set-purpose"

module Slack::Request
  class ChannelsSetPurposeParams < ParamsJSON
    property channel : String
    property purpose : String

    def initialize(@channel, @purpose)
    end
  end

  class ChannelsSetPurpose < Request
    def initialize(channel : String, purpose : String)
      super("channels.setPurpose", ChannelsSetPurposeParams.new(channel, purpose))
    end

    def get_response_class
      Slack::Response::ChannelsSetPurpose
    end
  end
end
