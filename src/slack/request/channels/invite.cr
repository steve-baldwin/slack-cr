require "../request"
require "../../response/channels/info"

module Slack::Request
  class ChannelsInviteParams < ParamsJSON
    property channel : String
    property user : String

    def initialize(@channel, @user)
    end
  end

  class ChannelsInvite < Request
    def initialize(channel : String, user : String)
      super("channels.invite", ChannelsInviteParams.new(channel, user))
    end

    def get_response_class
      Slack::Response::ChannelsInfo
    end
  end
end
