require "../request"

module Slack::Request
  class ChannelsUnarchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ChannelsUnarchive < Request
    def initialize(channel : String)
      super("channels.unarchive", ChannelsUnarchiveParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
