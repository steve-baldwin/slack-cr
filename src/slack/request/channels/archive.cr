require "../request"

module Slack::Request
  class ChannelsArchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ChannelsArchive < Request
    def initialize(channel : String)
      super("channels.archive", ChannelsArchiveParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
