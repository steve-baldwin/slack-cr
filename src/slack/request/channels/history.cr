require "../request"
require "../../response/channels/history"

module Slack::Request
  class ChannelsHistoryParams < Params
    property channel : String
    property count = 1000
    property unreads = true
    property inclusive = true
    property oldest = "0"
    property latest : String?

    def initialize(@channel, @count, @unreads, @inclusive, @oldest, @latest)
    end
  end

  class ChannelsHistory < Request
    def initialize(channel : String, count = 1000, unreads = true, inclusive = true, oldest = "0", latest : String? = nil)
      super("channels.history", ChannelsHistoryParams.new(channel, count, unreads, inclusive, oldest, latest))
    end

    def get_response_class
      Slack::Response::ChannelsHistory
    end
  end
end
