require "logger"
require "json"
require "http/client"
require "../response"

module Slack::Request
  SLACK_OAUTH_TOKEN  = "Bearer " + (ENV["SLACK_OAUTH_TOKEN"]? || "x")
  SLACK_BOT_TOKEN    = "Bearer " + (ENV["SLACK_BOT_TOKEN"]? || "x")
  SLACK_API_BASE_URL = "slack.com"
  extend self

  def get_client(use_bot = false)
    client = HTTP::Client.new(SLACK_API_BASE_URL, 443, true)
    client.before_request do |r|
      r.headers["Authorization"] = use_bot ? SLACK_BOT_TOKEN : SLACK_OAUTH_TOKEN
    end
    client
  end

  abstract class Params
    def content_type
      "application/x-www-form-urlencoded"
    end

    def serialize : String
      p = HTTP::Params.new
      {% for ivar in @type.instance_vars %}
      if {{ivar.name}}
      	p.add({{ivar.name.stringify}}, {{ivar.name}}.to_s)
    	end
      {% end %}
      p.to_s
    end
  end

  abstract class ParamsJSON < Params
    include JSON::Serializable

    def content_type
      "application/json; charset=utf-8"
    end

    def serialize : String
      self.to_json
    end
  end

  abstract class Request
    @client : HTTP::Client? = nil
    @@logger : Logger?

    @path : String
    @params : Params
    @response : Slack::Response?
    @deprecated = false
    property debug = false

    def self.logger=(logger : Logger)
      unless @@logger
        @@logger = logger
      end
      @@logger
    end

    def self.logger
      @@logger
    end

    def self.log(level, msg)
      if l = @@logger
        l.log(level, msg)
      end
    end

    def logger=(logger : Logger)
      Request.logger = logger
    end

    def log_debug(msg)
      Request.log(Logger::DEBUG, msg)
    end

    def log_info(msg)
      Request.log(Logger::INFO, msg)
    end

    def log_warn(msg)
      Request.log(Logger::WARN, msg)
    end

    def log_error(msg)
      Request.log(Logger::ERROR, msg)
    end

    def log_fatal(msg)
      Request.log(Logger::FATAL, msg)
    end

    def params
      @params
    end

    def initialize(@path, @params)
    end

    def submit(client : HTTP::Client? = nil)
      raise "Deprecated API" if @deprecated
      unless client
        unless @client
          @client = Slack::Request.get_client
        end
        client = @client
      end
      more = true
      headers = HTTP::Headers.new
      headers["Content-Type"] = @params.content_type
      while client && more
        log_info("Submitting #{@path}")
        params = @params.serialize
        log_info("params: #{params}") if @debug
        resp = client.post("/api/" + @path, headers, params)
        log_info("Got response: #{resp.status_code}")
        if resp.success?
          more = false
          #
          # Check whether or not the request was successful
          #
          if resp.body.includes?(%("ok":true))
            pp! resp.body if @debug
            # begin
            return get_response_class.from_json(resp.body)
            # rescue e : Exception
            #   raise "JSON parsing error"
            # end
          else
            failure = Slack::Response::Failure.from_json(resp.body)
            raise failure.error
          end
        else
          #
          # Handle throttling
          #
          if (resp.status_code == 429) && (wait_for = resp.headers["Retry-After"]?)
            log_warn("#{@path}: Got TOO_MANY_REQUESTS response. Waiting for #{wait_for} seconds before retry.")
            sleep wait_for.to_i.seconds
          else
            more = false
            raise "#{resp.status_code}: #{resp.body}"
          end
        end
      end
    end

    abstract def get_response_class
  end
end

require "./common/*"
