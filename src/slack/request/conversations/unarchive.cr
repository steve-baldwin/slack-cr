require "../request"

module Slack::Request
  class ConversationsUnarchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ConversationsUnarchive < Request
    def initialize(channel : String)
      super("conversations.unarchive", ConversationsUnarchiveParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
