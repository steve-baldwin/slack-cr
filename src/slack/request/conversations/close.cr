require "../request"

module Slack::Request
  class ConversationsCloseParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ConversationsClose < Request
    def initialize(channel : String)
      super("conversations.close", ConversationsCloseParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
