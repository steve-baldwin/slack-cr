require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsCreateParams < ParamsJSON
    property name : String
    property is_private = true
    property user_ids : String?

    def initialize(@name, @is_private, @user_ids)
    end
  end

  class ConversationsCreate < Request
    def initialize(name : String, is_private = true, user_ids : String? = nil)
      super("conversations.create", ConversationsCreateParams.new(name, is_private, user_ids))
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
