require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsInfoParams < Params
    property channel : String
    property include_locale = true

    def initialize(@channel, @include_locale)
    end
  end

  class ConversationsInfo < Request
    def initialize(channel : String, include_locale = true)
      super("conversations.info", ConversationsInfoParams.new(channel, include_locale))
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
