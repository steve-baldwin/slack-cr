require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsRenameParams < ParamsJSON
    property channel : String
    property name : String

    def initialize(@channel, @name)
    end
  end

  class ConversationsRename < Request
    def initialize(channel : String, name : String)
      super("conversations.rename", ConversationsRenameParams.new(channel, name))
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
