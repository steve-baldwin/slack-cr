require "../request"
require "../../response/conversations/members"

module Slack::Request
  class ConversationsMembersParams < Params
    property channel : String
    property cursor : String?
    property limit = 20

    def initialize(@channel, @cursor, @limit)
    end
  end

  class ConversationsMembers < Request
    def initialize(channel : String, cursor : String? = nil, limit = 20)
      super("conversations.members", ConversationsMembersParams.new(channel, cursor, limit))
    end

    def get_response_class
      Slack::Response::ConversationsMembers
    end
  end
end
