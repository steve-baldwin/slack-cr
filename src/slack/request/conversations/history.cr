require "../request"
require "../../response/conversations/history"

module Slack::Request
  class ConversationsHistoryParams < Params
    property channel : String
    property cursor : String?
    property inclusive = true
    property latest : String?
    property limit = 1000
    property oldest = "0"

    def initialize(@channel, @cursor, @inclusive, @latest, @limit, @oldest)
    end

    def initialize(@channel)
    end
  end

  class ConversationsHistory < Request
    def initialize(params : ConversationsHistoryParams)
      super("conversations.history", params)
    end

    def initialize(channel : String)
      initialize(ConversationsHistoryParams.new(channel))
    end

    def get_response_class
      Slack::Response::ConversationsHistory
    end
  end
end
