require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsInviteParams < ParamsJSON
    property channel : String
    property users : String

    def initialize(@channel, @users)
    end
  end

  class ConversationsInvite < Request
    def initialize(channel : String, users : String)
      super("conversations.invite", ConversationsInviteParams.new(channel, users))
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
