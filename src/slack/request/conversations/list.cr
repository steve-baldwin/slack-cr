require "../request"
require "../../response/conversations/list"

module Slack::Request
  class ConversationsListParams < ParamsJSON
    property cursor : String?
    property exclude_archived = true
    property limit = 20
    property types : String?

    def initialize(@cursor, @exclude_archived, @limit, @types)
    end
  end

  class ConversationsList < Request
    def initialize(cursor : String? = nil, exclude_archived = true, limit = 20, types : String? = nil)
      super("conversations.list", ConversationsListParams.new(cursor, exclude_archived, limit, types))
    end

    def get_response_class
      Slack::Response::ConversationsList
    end
  end
end
