require "../request"

module Slack::Request
  class ConversationsKickParams < ParamsJSON
    property channel : String
    property user : String

    def initialize(@channel, @user)
    end
  end

  class ConversationsKick < Request
    def initialize(channel : String, user : String)
      super("conversations.kick", ConversationsKickParams.new(channel, user))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
