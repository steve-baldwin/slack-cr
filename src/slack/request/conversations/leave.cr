require "../request"

module Slack::Request
  class ConversationsLeaveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ConversationsLeave < Request
    def initialize(channel : String)
      super("conversations.leave", ConversationsLeaveParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
