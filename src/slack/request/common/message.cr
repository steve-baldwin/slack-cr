require "./attachment"

module Slack::Request
  class ParamsMessage < ParamsJSON
    property channel : String
    property text : String?
    property as_user = false
    property attachments : Array(ParamsAttachment)?
    property link_names = true
    property parse = "none"

    def initialize(@channel, @text, @attachments)
      unless @text || @attachments
        raise "Must supply text or attachments for a message."
      end
    end
  end
end
