module Slack::Request
  class ParamsField
    include JSON::Serializable
    property short : Bool?
    property title : String?
    property value : String?
  end
end
