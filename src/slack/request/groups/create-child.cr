require "../request"
require "../../response/groups/info"

module Slack::Request
  class GroupsChildParams < Params
    property channel : String

    def initialize(@channel)
    end
  end

  class GroupsCreateChild < Request
    def initialize(channel : String)
      super("groups.createChild", GroupsChildParams.new(channel))
    end

    def get_response_class
      Slack::Response::GroupsInfo
    end
  end
end
