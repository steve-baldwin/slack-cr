require "../request"

module Slack::Request
  class GroupsKickParams < ParamsJSON
    property channel : String
    property user : String

    def initialize(@channel, @user)
    end
  end

  class GroupsKick < Request
    def initialize(channel : String, user : String)
      super("groups.kick", GroupsKickParams.new(channel, user))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
