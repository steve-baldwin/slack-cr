require "../request"

module Slack::Request
  class GroupsMarkParams < ParamsJSON
    property channel : String
    property ts : String

    def initialize(@channel, @ts)
    end
  end

  class GroupsMark < Request
    def initialize(channel : String, ts : String)
      super("groups.mark", GroupsMarkParams.new(channel, ts))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
