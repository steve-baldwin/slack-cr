require "../request"

module Slack::Request
  class GroupsUnarchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class GroupsUnarchive < Request
    def initialize(channel : String)
      super("groups.unarchive", GroupsUnarchiveParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
