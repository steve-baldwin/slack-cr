require "../request"
require "../../response/groups/open"

module Slack::Request
  class GroupsOpenParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class GroupsOpen < Request
    def initialize(channel : String)
      super("groups.open", GroupsOpenParams.new(channel))
    end

    def get_response_class
      Slack::Response::GroupsOpen
    end
  end
end
