module Slack::Request
  class GroupsLeaveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class GroupsLeave < Request
    def initialize(channel : String)
      super("groups.leave", GroupsLeaveParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
