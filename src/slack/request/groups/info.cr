require "../request"
require "../../response/groups/info"

module Slack::Request
  class GroupsInfoParams < Params
    property channel : String
    property include_locale = true

    def initialize(@channel)
    end
  end

  class GroupsInfo < Request
    def initialize(channel : String)
      super("groups.info", GroupsInfoParams.new(channel))
    end

    def get_response_class
      Slack::Response::GroupsInfo
    end
  end
end
