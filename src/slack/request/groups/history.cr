require "../request"
require "../../response/groups/history"

module Slack::Request
  class GroupsHistoryParams < Params
    property channel : String
    property count = 1000
    property inclusive = true
    property oldest = "0"
    property latest : String?
    property unreads = true

    def initialize(@channel, @count, @unreads, @inclusive, @oldest, @latest)
    end
  end

  class GroupsHistory < Request
    def initialize(channel : String, count = 1000, unreads = true, inclusive = true, oldest = "0", latest : String? = nil)
      super("groups.history", GroupsHistoryParams.new(channel, count, unreads, inclusive, oldest, latest))
    end

    def get_response_class
      Slack::Response::GroupsHistory
    end
  end
end
