require "../request"

module Slack::Request
  class GroupsArchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class GroupsArchive < Request
    def initialize(channel : String)
      super("groups.archive", GroupsArchiveParams.new(channel))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
