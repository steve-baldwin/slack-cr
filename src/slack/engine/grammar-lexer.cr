require "string_scanner"

module Slack::Engine
  class GrammarLexer
    RSTRING   = /("|'|‘|“)[^("|'|’|”)]*("|'|’|”)/
    RVARIABLE = /\{[^\}]*}/

    enum Type
      Constant
      String
      Variable
    end

    alias Token = NamedTuple(value: String, var: Variable, type: Type)
    alias Variable = NamedTuple(regex: Regex, after: ((String) -> (String | Nil)) | Nil)

    def self.lex(str : String, variables : Hash(String, Variable))
      result = Array(Token).new
      s = StringScanner.new(str)

      until s.eos?
        if s.check(/\w/) # Is this a constant?
          # Grab the constant
          r = s.scan(/\w*/).not_nil!
          result << {value: r, var: {regex: Regex.new(r.not_nil!), after: nil}, type: Type::Constant}
        elsif s.check(/\"/) # Is this the beginning of a string?
          r = s.scan(RSTRING)
          raise Exception.new("Unbounded string at #{s.offset + 1} in \"#{str}\"") if r == nil
          r = r.not_nil!
          r = r.lchop.rchop.lchop.rchop
          result << {value: r, var: {regex: RSTRING, after: nil}, type: Type::String}
        elsif s.check(/\{/) # Is this the beginning of a variable?
          r = s.scan(RVARIABLE)
          raise Exception.new("Unbounded variable declaration at #{s.offset + 1} in \"#{str}\"") if r == nil
          r = r.not_nil!
          r = r.lchop('{').rchop('}')
          variable = variables[r]? || {regex: /[^\s]*/, after: nil}
          result << {value: r, var: variable, type: Type::Variable}
        elsif s.check(/\s*/)
          s.scan(/\s*/)
        else
          raise Exception.new("Unrecognised sequence at #{s.offset + 1} in \"#{str}\"")
        end

        # Gobble whitespace or hit the end of the string
        s.scan(/\s*/)
      end

      result
    end
  end
end
