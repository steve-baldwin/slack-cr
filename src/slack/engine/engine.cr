require "./grammar-lexer"

module Slack::Command
  abstract def call(args : Hash(String, String), vars : Hash(String, String)) : String

  alias Block = (Hash(String, String), Hash(String, String)) -> (String)
end

module Slack::Engine
  class Exception < Exception
  end

  class Command
    getter tokens : Array(Slack::Engine::GrammarLexer::Token)
    getter command : Slack::Command | Slack::Command::Block
    getter async : Bool
    getter async_message : String?

    def initialize(@tokens, @command, @async, @async_message)
    end
  end

  class Engine
    alias Variable = NamedTuple(regex: Regex, after: ((String) -> (String | Nil)) | Nil)

    def initialize(@commands = Array(Command).new, @variables = Hash(String, Variable).new)
    end

    def add_command(grammar : String, command : Slack::Command | Slack::Command::Block, async = false, async_message : String? = nil)
      tokens = GrammarLexer.lex(grammar, @variables)

      @commands << Command.new(tokens, command, async, async_message)
    end

    def add_variable(name : String, regex : Regex, block : (String) -> (String | Nil))
      @variables[name] = {regex: regex, after: block}
    end

    def parse(input : String, commands : Array(Command)) : NamedTuple(command: Command, vars: Hash(String, String) | Nil) | Nil
      matches = Array(NamedTuple(command: Command, vars: Hash(String, String))).new
      commands.each do |command|
        vars = Hash(String, String).new
        remainder = input
        pos = 0
        command.tokens.each do |token|
          remainder = remainder.strip
          if value = token[:var][:regex].match remainder
            if token[:type] == GrammarLexer::Type::Variable
              vars[token[:value]] = value[0]
            elsif token[:type] == GrammarLexer::Type::String
              vars[token[:value]] = value[0].lchop.rchop
            end
            pos += 1
            remainder = value.post_match
            break if remainder.blank?
          end
        end
        matches << {command: command, vars: vars} if pos == command.tokens.size
      end
      return matches.max_by { |a| a[:command].tokens.size } if matches.size > 0
    end

    def execute(args : Hash(String, String)) : String
      result = parse(args["text"], @commands)
      if result
        result[:command].command.call(args, result[:vars])
      else
        Slack.config.dont_know
      end
    end
  end
end
