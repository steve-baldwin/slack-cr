require "../common/user-group"

module Slack::Response
  class UserGroupsList < Success
    property usergroups : Array(UserGroup)
  end
end
