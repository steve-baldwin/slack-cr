module Slack::Response
  class Reaction
    include JSON::Serializable
    property count : Int32
    property name : String
    property users : Array(String)
  end
end
