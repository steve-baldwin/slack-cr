module Slack::Response
  class File
    include JSON::Serializable
    property channels : Array(String)?
    property comments_count : Int32?
    property created : Int32?
    property display_as_bot : Bool?
    property editable : Bool?
    property external_type : String?
    property filetype : String?
    property groups : Array(String)?
    property id : String
    property image_exif_rotation : Int32?
    property ims : Array(String)?
    property is_external : Bool?
    property is_public : Bool?
    property mimetype : String?
    property mode : String?
    property name : String?
    property original_h : Int32?
    property original_w : Int32?
    property permalink : String?
    property permalink_public : String?
    property pinned_info : PinnedInfo?
    property pinned_to : Array(String)?
    property pretty_type : String?
    property public_url_shared : String?
    property reactions : Array(Reaction)?
    property size : Int32?
    property thumb_1024 : String?
    property thumb_1024_h : Int32?
    property thumb_1024_w : Int32?
    property thumb_160 : String?
    property thumb_360 : String?
    property thumb_360_h : Int32?
    property thumb_360_w : Int32?
    property thumb_480 : String?
    property thumb_480_h : Int32?
    property thumb_480_w : Int32?
    property thumb_64 : String?
    property thumb_720 : String?
    property thumb_720_h : Int32?
    property thumb_720_w : Int32?
    property thumb_80 : String?
    property thumb_800 : String?
    property thumb_800_h : Int32?
    property thumb_800_w : Int32?
    property thumb_960 : String?
    property thumb_960_h : Int32?
    property thumb_960_w : Int32?
    property timestamp : Int32?
    property title : String?
    property url_private : String?
    property url_private_download : String?
    property user : String?
    property username : String?
  end
end
