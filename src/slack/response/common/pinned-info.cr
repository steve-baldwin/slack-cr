module Slack::Response
  class PinnedInfo
    include JSON::Serializable
    property pinned_by : String
    property pinned_ts : Int32
  end
end
