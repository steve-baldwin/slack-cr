module Slack::Response
  class Comment
    include JSON::Serializable
    property comment : String?
    property created : Int32?
    property id : String?
    property is_intro : Bool?
    property pinned_info : PinnedInfo?
    property pinned_to : Array(String)?
    property reactions : Array(String)?
    property timestamp : Int32?
    property user : String?
  end
end
