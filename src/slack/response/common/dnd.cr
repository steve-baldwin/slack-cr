module Slack::Response
  class DND
    include JSON::Serializable
    property dnd_enabled : Bool
    property next_dnd_end_ts : Int32
    property next_dnd_start_ts : Int32
    property snooze_enabled : Bool
  end
end
