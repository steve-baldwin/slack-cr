module Slack::Response
  class DisplayCounts
    include JSON::Serializable
    property display_counts : Int32
    property guest_counts : Int32
  end
end
