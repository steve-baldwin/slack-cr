module Slack::Response
  class Attachment
    include JSON::Serializable
    property author_name : String?
    property author_link : String?
    property author_icon : String?
    property color : String?
    property fallback : String?
    property id : Int32
    property image_bytes : Int32?
    property image_height : Int32?
    property image_url : String?
    property image_width : Int32?
    property title : String?
    property title_link : String?
  end
end
