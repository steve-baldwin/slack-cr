module Slack::Response
  class InvitingUser
    include JSON::Serializable
    property id : String
    property is_app_user : Bool
    property is_restricted : Bool
    property is_ultra_restricted : Bool
    property name : String
    property profile : UserProfile
    property real_name : String
    property team_id : String
    property updated : Int32
  end
end
