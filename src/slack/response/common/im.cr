module Slack::Response
  class IM
    include JSON::Serializable
    property created : Int32
    property id : String
    property is_im : Bool
    property is_org_shared : Bool
    property is_user_deleted : Bool
    property priority : Int32?
    property user : String
  end
end
