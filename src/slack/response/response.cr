module Slack::Response
  abstract class Base
    include JSON::Serializable
    property ok : Bool
  end

  class StringItems
    include JSON::Serializable
    property items : Array(String)
  end

  class StringItemsType
    include JSON::Serializable
    property type : Array(String)
  end

  class CLV
    include JSON::Serializable
    property creator : String
    property last_set : Int32
    property value : String
  end

  class Success < Base
  end

  class Failure < Base
    property error : String
  end
end
