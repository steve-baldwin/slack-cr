require "../common/message"

module Slack::Response
  class ChannelsHistory < Success
    property has_more : Bool?
    property messages : Array(Message)
    property unread_count_display : Int32?
  end
end
