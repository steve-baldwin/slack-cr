require "../common/message"

module Slack::Response
  class ChannelsReplies < Success
    property has_more : Bool?
    property messages : Array(Message)
  end
end
