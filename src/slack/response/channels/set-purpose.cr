module Slack::Response
  class ChannelsSetPurpose < Success
    property purpose : String?
  end
end
