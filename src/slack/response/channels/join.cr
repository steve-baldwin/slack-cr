require "../common/conversation"

module Slack::Response
  class ChannelsJoin < Success
    property channel : Conversation
    property already_in_channel : Bool?
  end
end
