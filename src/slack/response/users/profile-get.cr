require "../common/user-profile"

module Slack::Response
  class UsersProfileGet < Success
    property profile : UserProfile
  end
end
