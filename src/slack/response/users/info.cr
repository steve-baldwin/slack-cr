require "../common/user"

module Slack::Response
  class UsersInfo < Success
    property user : User
  end
end
