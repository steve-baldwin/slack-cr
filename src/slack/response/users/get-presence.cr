module Slack::Response
  class UsersGetPresence < Success
    property auto_away : Bool?
    property connection_count : Int32?
    property last_activity : Int32?
    property manual_away : Bool?
    property online : Bool?
    property presence : String
  end
end
