module Slack::Response
  class GroupsSetTopic < Success
    property topic : String?
  end
end
