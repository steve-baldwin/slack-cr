require "../common/conversation"

module Slack::Response
  class GroupsInfo < Success
    property group : Conversation
  end
end
