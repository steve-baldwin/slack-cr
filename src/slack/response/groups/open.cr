module Slack::Response
  class GroupsOpen < Success
    property no_op : Bool?
    property already_open : Bool?
  end
end
