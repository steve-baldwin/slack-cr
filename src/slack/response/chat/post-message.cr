require "../common/message"

module Slack::Response
  class ChatPostMessage < Success
    property channel : String
    property ts : String
    property message : Message
  end
end
