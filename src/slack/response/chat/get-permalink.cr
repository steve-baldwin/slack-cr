module Slack::Response
  class ChatGetPermalink < Success
    property channel : String
    property permalink : String
  end
end
