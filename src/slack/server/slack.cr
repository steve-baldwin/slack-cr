require "logger"
require "./server"

module Slack
  extend self

  @@config = Config.new
  @@engine = Engine::Engine.new

  class Config
    property bot_name : String?
    property logger : Logger = Logger.new(STDOUT)
    property dont_know : String = "Beats me?"
  end

  def config(&block)
    yield @@config
  end

  def config
    @@config
  end

  def variable(name : String, regex : Regex, &block : (String) -> (String | Nil))
    @@engine.add_variable(name, regex, block)
  end

  def command(grammar : String, command : Slack::Command | Slack::Command::Block, async = false, async_message : String? = nil)
    @@engine.add_command(grammar, command, async, async_message)
  end

  def command(grammar : String, async = false, async_message : String? = nil, &command : Slack::Command::Block)
    self.command(grammar, command, async, async_message)
  end

  def run
    fn = ->execute(Hash(String, String))
    server = Slack::Server.new(fn)
    server.listen
  end

  protected def execute(args : Hash(String, String)) : String
    @@engine.execute(args)
  end

  protected def reset
    @@config = Config.new
    @@engine = Engine::Engine.new
  end
end
