class String
  def starts_with : String?
    self.split(" ")[0] if !self.blank?
  end
end
