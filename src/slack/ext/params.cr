module HTTP
  struct Params
    def fetch
      result = Hash(String, String).new
      raw_params.each do |params|
        result[params[0]] = params[1].first
      end
      result
    end
  end
end
