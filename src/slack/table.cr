module Slack
  PUNCTUATION = Set{'!', '.', '$', ',', '?', '%', ')', ']', '}', '*'}

  class Table
    @data : Array(Array(String))
    @rjust : Array(Bool)
    @width : Array(Int32)
    @header : Bool
    @cols = -1
    @rows = 0

    def initialize(@header = true)
      @data = [] of Array(String)
      @rjust = [] of Bool
      @width = [] of Int32
      @max_width = [] of Int32
      @newlines = [] of Int32
      @row_end = [] of Bool
      @have_multi_line = false
    end

    def add_row(row : Array(String))
      if @cols == -1
        @cols = row.size
        if @max_width.size > 0 && @cols != @max_width.size
          raise "Expecting #{@max_width.size} columns, found #{row.size}"
        end
        @rjust = Array.new(row.size, false)
        @width = Array.new(row.size, 0)
      else
        if row.size != @cols
          raise "Expecting #{@cols} columns, found #{row.size}"
        end
      end
      #
      # Do we need to split any of the column values due to max width exceeded?
      #
      work = [] of String
      row.each_with_index do |val, i|
        lines = [] of String
        max = @max_width[i]? || Int32::MAX
        max = Int32::MAX if max == 0
        val.split('\n').each do |line|
          len = line.size
          while line.size > max
            #
            # Search backward from the max
            #
            state = 'u'
            bpos = 0
            (0...max).reverse_each do |p|
              c = line[p]
              case state
              when 'u'
                if c.ascii_letter?
                  state = 'w'
                elsif c.ascii_whitespace?
                  state = 's'
                elsif PUNCTUATION === c
                  #
                  # Look forward in case the next character is a space
                  #
                  if line[p + 1].ascii_whitespace?
                    bpos = p + 1
                    break
                  end
                end
              when 'w'
                if c.ascii_whitespace?
                  state = 's'
                end
              when 's'
                if c.ascii_letter?
                  bpos = p + 1
                  break
                elsif PUNCTUATION === c
                  bpos = p + 1
                  break
                end
              end
            end
            lines << line[0, bpos].rstrip
            line = line[bpos, len].lstrip
          end
          lines << line
        end
        work << lines.join('\n')
      end

      max_nl = 0
      work.each_with_index do |val, i|
        nl = val.count('\n')
        max_nl = nl if nl > max_nl
      end
      #
      # Do we need to potentially add newlines to column values to even
      # things up?
      #
      if max_nl == 0
        @data << row
        @row_end << true
      else
        @have_multi_line = true
        (0..max_nl).each do |i|
          @row_end << (i == max_nl)
          tmp_row = [] of String
          work.each do |val|
            tmp_row << (val + ("\n" * (max_nl - val.count('\n')))).split('\n')[i]
          end
          @data << tmp_row
        end
      end
      @newlines << max_nl
    end

    def max_width=(cols : Array(Int32))
      raise "You need to set max_width before adding any rows" unless @data.size == 0
      @max_width = cols
    end

    def set_rjust(rcols : Array(Int32))
      raise "You need to call add_row at least once before calling set_rjust" if @cols < 1
      rcols.each do |col|
        if col >= @cols
          raise "col must be between 0 and #{@cols - 1}."
        end
        @rjust[col] = true
      end
    end

    def to_s
      #
      # Work out the max size of each column
      #
      @data.each do |row|
        row.each_with_index do |val, cnum|
          w = val.size
          @width[cnum] = w if w > @width[cnum]
        end
      end
      #
      # Now build the table
      #
      rowsep = String.build do |b|
        b << "\n"
        @width.each_with_index do |w, i|
          b << "+" if i > 0
          b << "-" * (w + 2)
        end
      end
      String.build do |b|
        @data.each_with_index do |row, rnum|
          row.each_with_index do |val, cnum|
            last_col = (cnum == (@cols - 1))
            pad = ""
            sz = val.size
            max_sz = @width[cnum]
            #
            # Determine the space padding. We don't pad the final column
            # unless it is right justified.
            #
            if (sz < max_sz) && (!last_col || @rjust[cnum])
              pad = " ".rjust(max_sz - sz)
            end
            b << '|' if cnum > 0
            b << " "
            if @rjust[cnum]
              b << pad + val
            else
              b << val + pad
            end
            b << " " unless last_col
          end
          if (@header && (rnum == 0)) || (@have_multi_line && @row_end[rnum])
            b << rowsep
          end
          b << "\n"
        end
      end
    end
  end
end
